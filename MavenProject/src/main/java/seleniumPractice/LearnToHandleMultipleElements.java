package seleniumPractice;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LearnToHandleMultipleElements {

	public static void main(String[] args) throws InterruptedException {

		// To setup Chrome driver
		WebDriverManager.chromedriver().setup();

		// To open the amazon application
		ChromeDriver driver = new ChromeDriver();

		// //to maximize the browser 
		
		driver.manage().window().maximize(); 
		
		//Open the Leaftaps URL through get method //
		driver.get("http://leaftaps.com/opentaps/control/main");
		
		List<WebElement> alltagname = driver.findElements(By.tagName("input"));
		System.out.println("The number of Input tags present are:" + alltagname.size());
		for (WebElement eachElement : alltagname) {
			String getId = eachElement.getAttribute("id");
			if(getId.equals("username")) {
				eachElement.sendKeys("demosalesmanager");
				System.out.println(getId);
			}
			
			
		}
		
		
	}

}
