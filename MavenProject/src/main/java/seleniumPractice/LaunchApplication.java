package seleniumPractice;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LaunchApplication {

	public static void main(String[] args) throws InterruptedException {

		// To setup Chrome driver
		WebDriverManager.chromedriver().setup();

		// To open the amazon application
		ChromeDriver driver = new ChromeDriver();

		// //to maximize the browser 
		
		driver.manage().window().maximize(); 
		
		//Open the Leaftaps URL through get method //
		driver.get("http://leaftaps.com/opentaps/control/main");
		
		WebElement username = driver.findElement(By.xpath("//input[@id= 'username']"));
		username.sendKeys("demosalesmanager");
		
		WebElement password = driver.findElement(By.xpath("//input[@id= 'password']"));
		password.sendKeys("crmsfa");
		
		WebElement loginButton = driver.findElement(By.xpath("//input[@class = 'decorativeSubmit']"));
		loginButton.click();
		
		
		WebElement crm = driver.findElement(By.xpath("//a[contains(text(),'CRM/SFA')]"));
		crm.click();
		
		WebElement lead = driver.findElement(By.xpath("//a[contains(text(),'Leads')]"));
		lead.click();
		
		WebElement createLead = driver.findElement(By.xpath("//a[contains(text(),'Create Lead')]"));
		createLead.click();
		
		WebElement companyName = driver.findElement(By.xpath("//input[@id= 'createLeadForm_companyName']"));
		companyName.sendKeys("TCS");
		
		WebElement firstName = driver.findElement(By.xpath("//input[@id= 'createLeadForm_firstName']"));
		firstName.sendKeys("Arul");
		
		WebElement lastName = driver.findElement(By.xpath("//input[@id= 'createLeadForm_lastName']"));
		lastName.sendKeys("Sridharan");
		
		WebElement source = driver.findElement(By.xpath("//select[@id= 'createLeadForm_dataSourceId']"));
		
		Select sourceDropDown = new Select(source);
		
		List<WebElement> listofItem = sourceDropDown.getOptions();
		
		for (WebElement options : listofItem) {
			
			WebElement getItem = listofItem.get(4);
			getItem.click();
			
		}
		
		WebElement marketing = driver.findElement(By.xpath("//select[@id= 'createLeadForm_marketingCampaignId']"));
		
		Select marketingDropDown = new Select(marketing);
		
		List<WebElement> items = marketingDropDown.getOptions();
		
		for (WebElement options : items) {
			
			WebElement getitem2 = items.get(3);
			getitem2.click();
		}
		
		
		WebElement createLeadButton = driver.findElement(By.xpath("//input[@class= 'smallSubmit']"));
		createLeadButton.click();
		
	}

}
