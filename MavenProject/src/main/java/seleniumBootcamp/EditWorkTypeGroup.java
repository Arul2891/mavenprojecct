package seleniumBootcamp;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EditWorkTypeGroup extends BaseClass {
	
	@Test
	public void editWorkTypGroup() throws InterruptedException  {
		
				Thread.sleep(7000);
				/*
				 * Actions clickWorkTypeGroup = new Actions(driver); WebElement
				 * clickWorkTypeGroup1 = driver.findElement(By.
				 * xpath("//input[@class='slds-input slds-text-color_default slds-p-left--none slds-size--1-of-1 input default input uiInput uiInputTextForAutocomplete uiInput--{remove}']"
				 * )); clickWorkTypeGroup1.sendKeys("Salesforce Automation by Arul Sridharan");
				 * clickWorkTypeGroup1.sendKeys(Keys.ENTER);
				 */
				
				/*
				 * Thread.sleep(7000); WebElement clickLink = driver.findElement(By.
				 * xpath("//a[@href= '/lightning/r/0VS2w000000ka2cGAA/view']"));
				 * JavascriptExecutor a = (JavascriptExecutor) driver;
				 * a.executeScript("arguments[0].click();", clickLink);
				 * 
				 * Thread.sleep(9000); WebElement editDropdown = driver.findElement(By.
				 * xpath("//a[@class= 'slds-grid slds-grid--vertical-align-center slds-grid--align-center sldsButtonHeightFix']"
				 * )); JavascriptExecutor e = (JavascriptExecutor) driver;
				 * e.executeScript("arguments[0].click();", editDropdown);
				 */
				
				/*
				 * List<WebElement> editOption = driver.findElements(By.
				 * xpath("//div[@class= 'branding-actions actionMenu popupTargetContainer uiPopupTarget uiMenuList forceActionsDropDownMenuList uiMenuList--left uiMenuList--default'"
				 * )); for(int i =1; i<=editOption.size(); i++) { WebElement getItem
				 * =editOption.get(2); JavascriptExecutor e1 = (JavascriptExecutor) driver;
				 * e1.executeScript("arguments[0],click()", getItem); }
				 */
				
				Thread.sleep(7000);
				Actions serchWorkType = new Actions(driver);
				WebElement searchWorkTypeGroups = driver.findElement(By.xpath("//input[@class= 'slds-input']"));
				searchWorkTypeGroups.sendKeys("Salesforce Automation by Arul Sridharan");
				searchWorkTypeGroups.sendKeys(Keys.ENTER);
				
				Thread.sleep(5000);
				driver.findElement(By.xpath("//span[@class= 'slds-icon_container slds-icon-utility-down']")).click();
				
				
				Thread.sleep(2000);
				WebElement editOption = driver.findElement(By.xpath("//div[contains(text(), 'Edit')]"));
				JavascriptExecutor e1 = (JavascriptExecutor) driver;
				e1.executeScript("arguments[0].click();", editOption);
				
				Thread.sleep(4000);
				WebElement description = driver.findElement(By.xpath("//textarea[@class = ' textarea']"));
				description.sendKeys("Automation");
				
				
				  WebElement groupType = driver.findElement(By.xpath("//a[@class= 'select']")); 
				  JavascriptExecutor g = (JavascriptExecutor) driver;
				  g.executeScript("arguments[0].click();", groupType);
				 
				  WebElement selectCapacity = driver.findElement(By.xpath("//a[contains(text(), 'Capacity')]"));
				  JavascriptExecutor cap = (JavascriptExecutor) driver;
				  cap.executeScript("arguments[0].click();", selectCapacity);
				  
				  driver.findElement(By.xpath("//button[@class = 'slds-button slds-button--neutral uiButton--brand uiButton forceActionButton']")).click();
				  
				  Thread.sleep(2000);
				  driver.findElement(By.xpath("//a[contains(text(), 'Salesforce Automation by Arul Sridharan')]")).click();
				   
				  // driver.findElement(By.xpath("//button[@class = 'slds-button slds-button--neutral uiButton--brand uiButton forceActionButton']")).click();
				  
				  Thread.sleep(3000);
				  WebElement getDescription = driver.findElement(By.xpath("//span[@class = 'uiOutputTextArea']"));
				  String descValue = getDescription.getText();
				  System.out.println(descValue);
				
				
	}

}
