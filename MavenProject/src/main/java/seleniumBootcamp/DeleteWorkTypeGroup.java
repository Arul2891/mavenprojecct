package seleniumBootcamp;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DeleteWorkTypeGroup extends BaseClass{

	@Test
	public void deleteWorkTypeGroup() throws InterruptedException {

		/*
		 * Thread.sleep(2000);
		 * driver.findElement(By.xpath("//a[@title = 'Work Type Groups']")).click();
		 */
		
		Thread.sleep(5000);
		Actions serchWorkType = new Actions(driver);
		WebElement searchWorkTypeGroups = driver.findElement(By.xpath("//input[@class= 'slds-input']"));
		searchWorkTypeGroups.sendKeys("Salesforce Automation by Arul Sridharan");
		searchWorkTypeGroups.sendKeys(Keys.ENTER);
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[@class= 'slds-icon_container slds-icon-utility-down']")).click();
		
		Thread.sleep(2000);
		WebElement clickDelete = driver.findElement(By.xpath("//div[contains(text(),'Delete')]"));
		JavascriptExecutor cDelete = (JavascriptExecutor) driver;
		cDelete.executeScript("arguments[0].click();",clickDelete);
		
		driver.findElement(By.xpath("//button[@class= 'slds-button slds-button--neutral uiButton--default uiButton--brand uiButton forceActionButton']")).click();
		
		Thread.sleep(1000);
		WebElement delete_toast_message = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']"));
		String pdeletemessage = delete_toast_message.getText();
		System.out.println(pdeletemessage);
	}

}
