package seleniumBootcamp;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass {
	
	
	ChromeDriver driver;
	
	@BeforeMethod
	public void preconditon() throws InterruptedException {
		// Opening the Chrome Browser
				//Set chrome options to disable the notifications
				ChromeOptions opt = new ChromeOptions();
		        opt.addArguments("--disable-notifications");
				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver(opt);
				driver.manage().window().maximize();
				

				// Opening the SaleForce URL with get method
				driver.get("https://login.salesforce.com");
				// Entering user name and passwordit
				driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
				driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
				driver.findElement(By.id("Login")).click();
				Thread.sleep(9000);
				// Clicking on Toggle App Launcher button on the left of the App
				driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
				Thread.sleep(9000);
				//Clicking on ViewAll button
				driver.findElement(By.xpath("//button[@class='slds-button']")).click();
				Thread.sleep(9000);
				//Clicking on WorkTypeGroup button
				WebElement workTypeGroup = driver.findElement(By.xpath("//p[text()='Work Type Groups']"));
				JavascriptExecutor j = (JavascriptExecutor) driver;
				j.executeScript("arguments[0].click();", workTypeGroup);
				

	}
	
	@AfterMethod
	public void postCondition() {
		driver.close();

	}

}
