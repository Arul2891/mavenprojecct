package seleniumBootcamp;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateWorkTypeGroup extends BaseClass {

		@Test
		public void createWorkTypeGroup() throws InterruptedException {

		Thread.sleep(9000);
		//driver.findElement(By.xpath("//span[@class='triggerLinkText selectedListView slds-page-header__title slds-truncate slds-p-right--xx-small uiOutputText']")).click();
		//driver.findElement(By.xpath("//div[@class='slds-context-bar__label-action slds-p-left--none slds-p-right--x-small'][1]")).click();
		driver.findElement(By.xpath("//a[@data-aura-class='forceActionLink']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//label[contains(@class,'label inputLabel')]/following-sibling::input")).sendKeys("Salesforce Automation by Arul Sridharan");
		driver.findElement(By.xpath("//button[@data-aura-class='uiButton forceActionButton'][3]")).click();
		Thread.sleep(2000);

		String toastMessage = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText(); 
		System.out.println(toastMessage);
		
	}
//
}
